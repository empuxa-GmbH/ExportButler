//
//  ERRule.m
//  Export Butler
//
//  Created by Sebastian Schmidt on 10.08.12.
/**
 Copyright (c) 2012 empuxa GmbH
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#import "ERRule.h"

@implementation ERRule
@synthesize expression;
@synthesize substitution;
@synthesize isActive;
@synthesize title;

- (id) initFromArray: (NSMutableArray*) array withTitle: (NSString*) ruleTitle{
        self = [super init];
    if (self){
        [self setExpression: (NSString *) [array objectAtIndex: 0]];
        [self setSubstitution: (NSString *) [array objectAtIndex: 1]];
        [self setIsActive: [[array objectAtIndex: 2] boolValue]];
        [self setTitle: ruleTitle];
    }
        return self;
};

- (NSString *) stringFromRuleAppliedTo: (NSString *) stringToProcess{
    if(isActive){
    NSError *errorReg = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern: expression
                                                                            options:NSRegularExpressionCaseInsensitive error:&errorReg];
        stringToProcess = [regex stringByReplacingMatchesInString:stringToProcess options:0 range:NSMakeRange(0, [stringToProcess length]) withTemplate:substitution];
    };
    return stringToProcess;
};
-(NSMutableArray *) toStringArray{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSArray *stringArray = @[[self title], [self expression],[self substitution],([self isActive]? @"YES" : @"NO")];
    NSLog(@"%@", stringArray);
    [array setArray: stringArray];
    return array;
}

- (id) initFromArray: (NSArray*) array{
    self = [super init];
    if (self){
        [self setTitle: (NSString *) [array objectAtIndex: 0]];
        [self setExpression: (NSString *) [array objectAtIndex: 1]];
        [self setSubstitution: (NSString *) [array objectAtIndex: 2]];
        [self setIsActive: [((NSString *) [array objectAtIndex: 3]) boolValue]];
    }
    return self;
};
@end
