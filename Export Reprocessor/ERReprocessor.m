//
//  ERReprocessor.m
//  Export Reprocessor
//
//  Created by Sebastian Schmidt on 07.08.12.
/**
 Copyright (c) 2012 empuxa GmbH
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#import "ERReprocessor.h"
#import "ERSystemOfRules.h"
#import "ERRule.h"
#import "parseCSV.h"

@implementation ERReprocessor
@synthesize rules;

- (void) reprocess: (NSString *) filePath
{
    NSLog(@"Reprocessing: %@", filePath);
    
    NSError *__autoreleasing * error = nil;
    NSStringEncoding encoding;
    NSString *stringToProcess = [[NSString alloc] initWithContentsOfFile: filePath usedEncoding: &encoding  error: error];
    // Dump whole file
    NSLog(@"%@",stringToProcess);
    
    //Apply rules
    for (ERRule *rule in [rules getRulesToApply]) {
        if([rule isActive]){
            stringToProcess = [rule stringFromRuleAppliedTo: stringToProcess];
        }
    }
     
    // Dump whole result
    NSLog(@"%@",stringToProcess);
    [stringToProcess writeToFile:filePath atomically: YES encoding: encoding error: error];
}

- (id) initWithRules: (ERSystemOfRules *) initRules{
    self = [super init];
    if (self){
        [self setRules: initRules];
    }
    return self;
}
    @end