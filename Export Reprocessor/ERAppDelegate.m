//
//  ERAppDelegate.m
//  Export Reprocessor
//
//  Created by Sebastian Schmidt on 06.08.12.
/**
 Copyright (c) 2012 empuxa GmbH
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#import "ERAppDelegate.h"
#import "ERDropView.h"
#import "ERSystemOfRules.h"
#import "ERRule.h"
#import "ERTabViewDelegate.h"

@implementation ERAppDelegate
@synthesize butlerTabView;
@synthesize dropSystemSelector;
@synthesize expressionsSystemSelector;
@synthesize expressionsRuleSelector;
@synthesize dropZone;
@synthesize systemsOfRules;
@synthesize tabViewDelegate;
@synthesize expression;
@synthesize substitution;
@synthesize activeSystem;
@synthesize expressionIsActive;
@synthesize writeToFolder;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    systemsOfRules = [[NSMutableArray alloc] initWithCapacity: 100];
    NSMutableString *writePath;
    BOOL fileExists;
    BOOL loadedSets = NO;
    NSFileManager *fileManager;
    NSArray *filelist;
    
    writePath = [[NSMutableString alloc] initWithString: NSHomeDirectory()];
    writeToFolder = @"/ExportButler";
    [writePath appendString: writeToFolder];
    fileManager = [NSFileManager defaultManager];
    fileExists = [fileManager fileExistsAtPath: writePath];
    if(fileExists){
        NSLog(@"ExportButler Folder existed");
        filelist = [fileManager contentsOfDirectoryAtPath: writePath error: nil];
        NSInteger count;
        NSInteger i;
        
        count = [filelist count];
        
        for (i = 0; i < count; i++){
            NSString *file = [filelist objectAtIndex: i];
            NSMutableString *currentPath = [[NSMutableString alloc] initWithString: writePath];
            [currentPath appendString: @"/"];
            [currentPath appendString: file];
            NSLog (@"%@", currentPath);
            if(! [file isEqualTo: @".DS_Store"]){
                ERSystemOfRules *system = [[ERSystemOfRules alloc] initFromFile: [currentPath description] withTitle: [file stringByDeletingPathExtension]];
                [systemsOfRules addObject: system];
                loadedSets = YES;
            }
        }
    }
    if (! loadedSets){
        ERSystemOfRules *initialAC = [[ERSystemOfRules alloc] initWithHardcodedActiveCollabRules];
        [self setActiveSystem: initialAC];
        ERSystemOfRules *initialExample = [[ERSystemOfRules alloc] initWithHardcodedExample];
        [systemsOfRules addObject: initialAC];
        [systemsOfRules addObject: initialExample];
    }
    tabViewDelegate = [[ERTabViewDelegate alloc] init];
    [butlerTabView setDelegate: tabViewDelegate];
    [self updateUIForChangedSystemsOfRules];
}

-(void)updateUIForChangedSystemsOfRules{
    [dropSystemSelector removeAllItems];
    [expressionsSystemSelector removeAllItems];
    Boolean first = YES;
    for(ERSystemOfRules *system in systemsOfRules){
        if(first){
            [self setActiveSystem: system];
        }
        [dropSystemSelector addItemWithTitle: [system title]];
        [expressionsSystemSelector addItemWithTitle: [system title]];
        first = NO;
    }
    [self updateUIForChangedSelectionOnSystemOfRules];
    //NSLog(@"%@",[[dropSystemSelector selectedItem] title]);
    //NSLog(@"%@",[[expressionsSystemSelector selectedItem] title]);
}

-(void)updateUIForChangedSelectionOnSystemOfRules{
    [expressionsRuleSelector removeAllItems];
    ERSystemOfRules *system = [self activeSystem];
    for(ERRule *rule in [system getRulesToApply]){
        [expressionsRuleSelector addItemWithTitle: [rule title]];
    }
    [self updateUIForChangedRule];
}

-(void)updateUIForChangedRule{
    NSMenuItem *item = [expressionsRuleSelector selectedItem];
    NSInteger index = [expressionsRuleSelector indexOfItem: item];
    ERRule *rule = [[activeSystem getRulesToApply] objectAtIndex: index];
    [expression setStringValue: [rule expression]];
    [substitution setStringValue: [rule substitution]];
    if([rule isActive]){
        [expressionIsActive setState: NSOnState];
    } else {
        [expressionIsActive setState: NSOffState];
    }
}

- (IBAction)selectedSetChanged:(id)sender {
    [self setActiveSystem: [[self systemsOfRules] objectAtIndex: [((NSPopUpButton *) sender) indexOfItem: [((NSPopUpButton *) sender) selectedItem]]]];
    [self updateUIForChangedSelectionOnSystemOfRules];
    [self syncSetSelectorsWithChangeOn: sender];
}

- (IBAction)selectedRuleChanged:(id)sender {
    [self updateUIForChangedRule];
}

- (IBAction)saveSelectedSet:(id)sender {
    [self updateSelectedSetAndRuleFromUI];
    [activeSystem saveToCSV];
}

- (void) updateSelectedSetAndRuleFromUI{
    ERRule *selectedRule = [[activeSystem expressions] objectAtIndex: [expressionsRuleSelector indexOfItem: [expressionsRuleSelector selectedItem]]];
    [selectedRule setExpression: [expression stringValue]];
    [selectedRule setSubstitution: [substitution stringValue]];
    [selectedRule setIsActive: [[expressionIsActive stringValue] boolValue]];
}

-(void)syncSetSelectorsWithChangeOn: (id) sender{
    NSPopUpButton *buttonSelectionChanged = (NSPopUpButton *) sender;
    NSPopUpButton *buttonToSync = ( (buttonSelectionChanged == dropSystemSelector) ? expressionsSystemSelector : dropSystemSelector);
    [buttonToSync selectItemAtIndex: [buttonSelectionChanged indexOfItem: [buttonSelectionChanged selectedItem]]];
}
@end
