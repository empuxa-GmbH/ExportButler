//
//  ERSystemOfRules.m
//  Export Butler
//
//  Created by Sebastian Schmidt on 10.08.12.
/**
 Copyright (c) 2012 empuxa GmbH
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#import "ERSystemOfRules.h"
#import "ERRule.h"
#import "CHCSV.h"

@implementation ERSystemOfRules
@synthesize expressions;
@synthesize writeToFolder;
@synthesize title;
@synthesize loadedFromFile;

- (NSMutableArray *) getRulesToApply{
    return expressions;
};

- (id) init{
    self = [super init];
    if (self){
        [self setLoadedFromFile: NO];
        [self setWriteToFolder: @"/ExportButler"];
    }
    return self;
}

- (id) initFromUI{
    return nil;
};
- (id) initWithHardcodedActiveCollabRules{
    
    /*
     Later this should be used like
     NSError *errorReg = NULL;
     NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:
     options:NSRegularExpressionCaseInsensitive error:&errorReg];
     and
     */
    self = [self init];
    if (self){
        expressions = [[NSMutableArray alloc] initWithCapacity:100];
        NSMutableArray *expression1 = [NSMutableArray arrayWithCapacity: 3];
        [expression1 addObject: @"&amp;"];
        [expression1 addObject: @"&"];
        [expression1 addObject: [NSNumber numberWithBool: YES]];
        ERRule *rule1 = [[ERRule alloc] initFromArray: expression1 withTitle: @"Ampersand"];
        [expressions addObject: rule1];
        
        NSMutableArray *expression2 = [NSMutableArray arrayWithCapacity: 3];
        [expression2 addObject: @"&quot;"];
        [expression2 addObject: @"\""];
        [expression2 addObject: [NSNumber numberWithBool: YES]];
        ERRule *rule2 = [[ERRule alloc] initFromArray: expression2 withTitle: @"Quotation"];
        [expressions addObject: rule2];
        
        NSMutableArray *expression3 = [NSMutableArray arrayWithCapacity: 3];
        [expression3 addObject: @","];
        [expression3 addObject: @";"];
        [expression3 addObject: [NSNumber numberWithBool: YES]];
        ERRule *rule3 = [[ERRule alloc] initFromArray: expression3 withTitle: @"Comma"];
        [expressions addObject: rule3];
        
        NSMutableArray *expression4 = [NSMutableArray arrayWithCapacity: 3];
        [expression4 addObject: @";([0-9]*)\\.([0-9]*);"];
        [expression4 addObject: @";$1,$2;"];
        [expression4 addObject: [NSNumber numberWithBool: YES]];
        ERRule *rule4 = [[ERRule alloc] initFromArray: expression4 withTitle: @"Money Values"];
        [expressions addObject: rule4];
    }
    [self setLoadedFromFile: NO];
    self.title = @"Active Collab";
    return self;
};
- (id) initWithHardcodedExample{
    
    /*
     Later this should be used like
     NSError *errorReg = NULL;
     NSRegularExpression *regex1 = [NSRegularExpression regularExpressionWithPattern:
     options:NSRegularExpressionCaseInsensitive error:&errorReg];
     and
     */
    self = [self init];
    if (self){
        expressions = [[NSMutableArray alloc] initWithCapacity:100];
        NSMutableArray *expression1 = [NSMutableArray arrayWithCapacity: 3];
        [expression1 addObject: @"&amp;"];
        [expression1 addObject: @"&"];
        [expression1 addObject: [NSNumber numberWithBool: NO]];
        ERRule *rule1 = [[ERRule alloc] initFromArray: expression1 withTitle: @"ExampleRule1"];
        [expressions addObject: rule1];
        
        NSMutableArray *expression2 = [NSMutableArray arrayWithCapacity: 3];
        [expression2 addObject: @"&quot;"];
        [expression2 addObject: @"\""];
        [expression2 addObject: [NSNumber numberWithBool: NO]];
        ERRule *rule2 = [[ERRule alloc] initFromArray: expression2 withTitle: @"ExampleRule2"];
        [expressions addObject: rule2];
        
        NSMutableArray *expression3 = [NSMutableArray arrayWithCapacity: 3];
        [expression3 addObject: @","];
        [expression3 addObject: @";"];
        [expression3 addObject: [NSNumber numberWithBool: YES]];
        ERRule *rule3 = [[ERRule alloc] initFromArray: expression3 withTitle: @"ExampleRule3"];
        [expressions addObject: rule3];
        
        NSMutableArray *expression4 = [NSMutableArray arrayWithCapacity: 3];
        [expression4 addObject: @";([0-9]*)\\.([0-9]*);"];
        [expression4 addObject: @";$1,$2;"];
        [expression4 addObject: [NSNumber numberWithBool: NO]];
        ERRule *rule4 = [[ERRule alloc] initFromArray: expression4 withTitle: @"ExampleRule4"];
        [expressions addObject: rule4];
    }
    
    [self setLoadedFromFile: NO];
    self.title = @"Example";
    return self;
};

-(BOOL) saveToCSV{
    NSMutableString *writePath;
    BOOL fileExists;
    NSFileManager *fileManager;
    
    writePath = [[NSMutableString alloc] initWithString: NSHomeDirectory()];
    [writePath appendString: writeToFolder];
    fileManager = [NSFileManager defaultManager];
    fileExists = [fileManager fileExistsAtPath: writePath];
    if(! fileExists){
        [fileManager createDirectoryAtPath:writePath withIntermediateDirectories:YES attributes:nil error:NULL];
    }
    [writePath appendString: @"/"];
    [writePath appendString: [self title]];
    [writePath appendString: @".csv"];
    
    fileManager = [NSFileManager defaultManager];
    NSError *error;
    fileExists = [fileManager fileExistsAtPath: writePath];
    if (fileExists)
    {
        [fileManager removeItemAtPath: writePath error:&error];
    }
    NSLog(@"%@",writePath);
    CHCSVWriter *writer = [[CHCSVWriter alloc] initWithCSVFile: writePath atomic: NO];
    [writer setDelimiter: @"\t"];
    for(ERRule *rule in expressions){
        [writer writeLineWithFields: [rule toStringArray]];
    }
    return YES;
}

- (id) initFromFile: (NSString *) file withTitle: (NSString *) newTitle{
    self = [self init];
    if (self){
        NSStringEncoding encoding = 0;
        NSError *error = nil;
        expressions = [[NSMutableArray alloc] init];
        title = [[NSString alloc] init];
        title = newTitle;
        NSArray *rows = [[NSArray alloc] initWithContentsOfCSVFile: file encoding:encoding delimiter: @"\t" error:&error];
        if (rows == nil) {
            //something went wrong; log the error and exit
            NSLog(@"error parsing file: %@", error);
        } else {
            for (NSArray *row in rows) {
                ERRule *ruleFromFile = [[ERRule alloc] initFromArray: row];
                [[self expressions] addObject: ruleFromFile];
            }
        }
    }
    [self setLoadedFromFile: YES];
    return self;
}
@end
