//
//  ERDropView.m
//  Export Reprocessor
//
//  Created by Sebastian Schmidt on 07.08.12.
/**
 Copyright (c) 2012 empuxa GmbH
 
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished
 to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 **/

#import "ERDropView.h"
#import "ERReprocessor.h"
#import "ERAppDelegate.h"


@implementation ERDropView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        highlighted = NO;
        NSBundle *bundle = [NSBundle mainBundle];
        NSImage *image = [bundle imageForResource: @"1344254724_document"];
        [self setImage: image];
        [self registerForDraggedTypes:
         [NSArray arrayWithObject:NSFilenamesPboardType]];
    }
    
    return self;
}

- (void)setHighlighted:(BOOL)aHighlighted {
    highlighted = aHighlighted;
    [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)frame {
    
    //TODO Improve using layer
    
    /*
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [NSColor greenColor].CGColor;
     */
    
   highlighted ? [[NSColor grayColor] set] : [[NSColor clearColor] set];
    [NSBezierPath setDefaultLineWidth:4.0];
    if (highlighted) {
        [super drawRect:frame];
        [NSBezierPath strokeRect:frame]; // will give a 2 pixel wide border
    } else {
        [NSBezierPath fillRect:frame];
        [super drawRect:frame];
    }
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {
    if ([sender draggingSource] == self) {
        return NSDragOperationNone;
    }else
    [self setHighlighted: YES];
    return NSDragOperationCopy;
    }

- (void)draggingExited:(id <NSDraggingInfo>)sender
{
    [self setHighlighted: NO];
}

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
    return YES;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
    NSPasteboard *pb = [sender draggingPasteboard];
    if(![self readFromPasteboard:pb]) {
        NSLog(@"Error: Could not read from dragging pasteboard");
        return NO;
    }
    return YES;
}

- (void)concludeDragOperation:(id <NSDraggingInfo>)sender
{
    [self setHighlighted: NO];
}

- (BOOL)readFromPasteboard:(NSPasteboard *)pb
{
    NSArray *files = [pb propertyListForType:NSFilenamesPboardType];
    ERAppDelegate *delegate = (ERAppDelegate *)[[NSApplication sharedApplication] delegate];
    if ([files count] > 0)
    {
        for (id file in files) {
            NSString *filename = (NSString*) file;
            if([filename length] > 1){
                ERReprocessor *reprocessor = [[ERReprocessor alloc] initWithRules: [[delegate systemsOfRules] objectAtIndex: 0]];
                [reprocessor reprocess: filename];
            }
        }

        return YES;
    }
    NSLog(@"Called with empty pasteboard");
    return NO;
}
@end
