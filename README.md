ExportButler Version 0.12
========================================================================================================================

What is ExportButler?
------------------------------------------------------------------------------------------------------------------------

ExportButler is a small application that processes CSV-Files and runs predefined operations on the data of the files.
We use the Butler to prepare our ActiveCollab-Exports for import to Excel.

> Please note that the whole thing might be a little raw due to the fact that this has been my first OS X application.
> I will try to polish it with the next releases as soon as there is some time for it.
